import _Vue from "Vue";
import IgCrud from "@/modules/ig-crud/components/IgCrud.vue";

export const IgCrudModule = (Vue: typeof _Vue) => {
  Vue.component("IgCrud", IgCrud);
};
