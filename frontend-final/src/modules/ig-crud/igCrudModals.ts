import { create } from "vue-modal-dialogs";
import IgConfirmDeleteModal from "@/modules/ig-crud/components/modals/IgConfirmDeleteModal.vue";
import IgCreateItemModal from "@/modules/ig-crud/components/modals/IgCreateItemModal.vue";
import IgEditItemModal from "@/modules/ig-crud/components/modals/IgEditItemModal.vue";

let igConfirmDeleteModal = create(IgConfirmDeleteModal);
let igCreateItemModal = create(IgCreateItemModal, "component");
let igEditItemModal = create(IgEditItemModal, "component", "item");

export { igConfirmDeleteModal, igCreateItemModal, igEditItemModal };
