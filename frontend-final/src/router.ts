import Vue from "vue";
import Router from "vue-router";

import App from "./views/App.vue";
import AppBrowser from "./views/AppBrowser.vue";
import AppBrowserBooks from "./views/AppBrowserBooks.vue";
import AppBrowserAuthors from "./views/AppBrowserAuthors.vue";
import AppAdmin from "./views/AppAdmin.vue";
import AppAdminBooks from "./views/AppAdminBooks.vue";
import AppAdminAuthors from "./views/AppAdminAuthors.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  routes: [
    {
      path: "/app",
      name: "app",
      component: App,
      children: [
        {
          path: "browser",
          name: "app.browser",
          component: AppBrowser,
          children: [
            {
              path: "books",
              name: "app.browser.books",
              component: AppBrowserBooks
            },
            {
              path: "authors",
              name: "app.browser.authors",
              component: AppBrowserAuthors
            }
          ]
        },
        {
          path: "admin",
          name: "app.admin",
          component: AppAdmin,
          children: [
            {
              path: "books",
              name: "app.admin.books",
              component: AppAdminBooks
            },
            {
              path: "authors",
              name: "app.admin.authors",
              component: AppAdminAuthors
            }
          ]
        }
      ]
    }
  ]
});
