import axios, { AxiosRequestConfig, Method } from "axios";
import { IRequest } from "@/services/request";
import BaseModel from "@/services/models/BaseModel";

export default class Api {
  books: BaseModel;
  authors: BaseModel;

  constructor(private request: IRequest) {
    this.books = new BaseModel("/books", this.request);
    this.authors = new BaseModel("/authors", this.request);
  }
}
