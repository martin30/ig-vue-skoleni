import { IRequest } from "@/services/request";

export default class BaseModel {
  constructor(private endpoint: string, private request: IRequest) {}

  getAll() {
    return this.request("get", this.endpoint);
  }

  getOne(id: number) {
    return this.request("get", `${this.endpoint}/${id}`);
  }

  create(data: any) {
    return this.request("post", this.endpoint, data);
  }

  edit(id: number, data: any) {
    return this.request("put", `${this.endpoint}/${id}`, data);
  }

  remove(id: number) {
    return this.request("delete", `${this.endpoint}/${id}`);
  }
}
