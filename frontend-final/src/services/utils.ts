export function optionsFormatter(
  list: any[],
  textFormatter: (item: any) => string,
  valueProp: string = "id"
): any {
  return list.map(item => ({
    text: textFormatter(item),
    value: item[valueProp]
  }));
}
