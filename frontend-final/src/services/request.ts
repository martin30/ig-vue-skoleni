import axios, { AxiosRequestConfig, Method } from "axios";

export type IRequest = (method: Method, endpoint: string, data?: any) => any;

export function requestFactory(baseUrl: string) {
  return function(method: Method, endpoint: string, data?: any): Promise<any> {
    let requestConfig: AxiosRequestConfig = {
      method,
      url: baseUrl + endpoint,
      data
    };

    return axios(requestConfig).then(res => res.data);
  };
}
