import Vue from "vue";
import Main from "./Main.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import * as ModalDialogs from "vue-modal-dialogs";

import Api from "@/services/api";
import { IRequest } from "@/services/request";
import { IgCrudModule } from "@/modules/ig-crud";
import { requestFactory } from "@/services/request";

declare module "vue/types/vue" {
  interface Vue {
    $igRequest: IRequest;
    $api: Api;
  }
}

const request = requestFactory("http://localhost:1337");
Vue.prototype.$igRequest = request;
Vue.prototype.$api = new Api(request);

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(IgCrudModule);
Vue.use(ModalDialogs);

new Vue({
  router,
  store,
  render: h => h(Main)
}).$mount("#app");
