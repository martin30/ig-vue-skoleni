// https://jsbin.com/sasubenecu/edit?js,console

// primitives
let x = ""
let y = 1
let z = false
let a = undefined
let b = null

// object
let obj = {}
let obj2 = new Object();

// this
let testObj = {
  testString: "testString",

  testMethod: function (name) {
    console.log(name + ' ' + this.testString)
  }
}
testObj.testMethod('testObj');

let testObj2 = {
  testMethod: testObj.testMethod.bind(testObj)
}
testObj2.testMethod("testObj2")

let testObj3 = {
  testMethod: testObj.testMethod
}
testObj3.testMethod.apply(testObj, ['testObj3']);

let testObj4 = {
  testMethod: function (name) { return testObj.testMethod(name); }
}
testObj4.testMethod('testObj4');

// let vs const vs var
let i, canDelete, userName;
const REFRESH_INTERVAL = 0;
const updateUser = () => {}

// arrow function
let arr = [{ ready: true, text: "tom hanks"}, {ready: false, text: "coffee"}, {ready: true, text: "book"}];
let names = arr
  .filter(el => el.ready)
  .map(el => (console.log(el), el.text));
console.log(names)

let testObj5 = {
  testString: "hello",
  testMethod() {
    function x1() {
      console.log("inside normal " + this.testString)
    }

    const x2 = () => {
      console.log("inside arrow " + this.testString)
    }

    x1();
    x2();
  }
}
testObj5.testMethod()

// rest, spread
let spreadObj = { a: 1, b: 2, c: 3};
let clonedObj = {...spreadObj, a: 2};
console.log(clonedObj)

let restFn = (a, ...restArgs) => console.log(restArgs);
restFn(1, 2, 3, 4)

// template string
let testVar1 = "ahoj";
let testVar2 = "test";
let templateString = `Vypis promennych ${testVar1} v textu. ${testVar2}
muze 
byt 
na vic radku`;
console.log(templateString);

// property shorthand
let name = "Jan Novak"
let age = 30
let city = "Prague"
let user = { name, age, city, delete() { } }
console.log(user)

// destructuring
let book = { name: "Javascript: The good parts", pages: 100, author: "Jan Hus" }
let { pages, author, ...restOfBook } = book;
console.log(pages, author, restOfBook)

let entityDetails = ["book", 45, false];
let [entityName, entityId, deletable] = entityDetails;
console.log(entityName, entityId, deletable)

let callable = ({name, age}) => { console.log(name, age)}
callable({ name: "Jindrich Novotny", age: 45})

// promise
let waitRejected = (wait) => new Promise((res, rej) => setTimeout(rej, wait))
let waitPromise = (wait) => new Promise((res) => setTimeout(res, wait))
waitPromise(200).then(() => console.log("finished promise"))
console.log("next line after promise")

waitPromise(200)
  .then(() => waitPromise(200))
  .then(() => waitPromise(200))
  .then(() => console.log("waited for 600 total"))

waitPromise(200)
  .then(() => waitPromise(200))
  .then(() => waitRejected(200))
  .then(() => waitPromise(200))
  .then(() => console.log("this will not be shown"))
  .then(() => {}, () => console.log("rejected"))
  .catch(() => console.log("already caught, will not be shown"))



// class
class Furniture {
  material;

  constructor(_material) {
    this.material = _material;
  }

  printMaterial() {
    console.log(this.material);
  }
}


class Table extends Furniture {
  _legs = 4;

  get legs() {
    console.log("legs accessed")
    return this._legs;
  }

  constructor(_material) {
    super(_material)
  }

  printLegs() {
    console.log(this.legs)
  }
}

let worktable = new Table("wood")
worktable.printMaterial()
worktable.printLegs()
























