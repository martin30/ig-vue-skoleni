export default function(author: any) {
  return `${author.name} ${author.surname}`;
}
