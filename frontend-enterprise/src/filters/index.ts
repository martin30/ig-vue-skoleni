import _Vue from "Vue";
import authorName from "@/filters/authorName";

export default function(Vue: typeof _Vue) {
  Vue.filter("authorName", authorName);
}
