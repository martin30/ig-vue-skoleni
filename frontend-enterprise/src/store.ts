import Vue from "vue";
import Vuex from "vuex";
import { IRequest } from "@/services/request";

Vue.use(Vuex);

export default ($request: IRequest) =>
  new Vuex.Store({
    state: {
      bookList: []
    },
    mutations: {
      setBooks(state, books) {
        state.bookList = books;
      }
    },
    actions: {
      loadAllBooks({ commit }) {
        return $request("GET", "/books").then(res => {
          commit("setBooks", res);
        });
      },
      loadByAuthor({ commit }, authorId) {
        return $request("GET", `/books?author=${authorId}`).then(res => {
          commit("setBooks", res);
        });
      }
    }
  });
