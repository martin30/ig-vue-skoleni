import _Vue from "Vue";
import elementContent from "@/directives/elementContent";

export default function(Vue: typeof _Vue) {
  Vue.directive("elementContent", elementContent());
}
