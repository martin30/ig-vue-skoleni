export default () => {
  let map = new WeakMap();

  return {
    bind($el: any, binding: any) {
      let intval = setInterval(() => {}, 1000);

      map.set($el, intval);
      $el.innerHTML = binding.value;
    },
    update($el: any, binding: any) {
      $el.innerHTML = binding.value;
    },
    unbind($el: any) {
      clearInterval(map.get($el));
    }
  };
};
