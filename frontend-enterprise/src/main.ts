import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import * as VueModalDialogs from "vue-modal-dialogs";

import { IRequest } from "@/services/request";

import Main from "./Main.vue";
import router from "./router";
import storeFactory from "./store";
import IgCrud from "./modules/ig-crud";
import requestFactory from "./services/request";
import Api from "./services/models/api";
import filters from "./filters";
import directives from "./directives";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(IgCrud);
Vue.use(VueModalDialogs);
Vue.use(filters);
Vue.use(directives);

let $request = requestFactory("http://localhost:1337");
let $api = new Api($request);
let store = storeFactory($request);

declare module "vue/types/vue" {
  interface Vue {
    $igRequest: IRequest;
    $igApi: Api;
  }
}
Vue.prototype.$igRequest = $request;
Vue.prototype.$igApi = $api;

new Vue({
  router,
  store,
  render: h => h(Main)
}).$mount("#app");
