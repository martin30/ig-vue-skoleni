import Vue from "vue";
import Router from "vue-router";
import App from "./views/App.vue";
import AppPublic from "./views/AppPublic.vue";
import AppPublicAll from "./views/AppPublicAll.vue";
import AppPublicBook from "./views/AppPublicBook.vue";
import AppAdmin from "./views/AppAdmin.vue";
import AppAdminBooks from "./views/AppAdminBooks.vue";
import AppAdminAuthors from "./views/AppAdminAuthors.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "app",
      component: App,
      redirect: { name: "app.public" },
      children: [
        {
          path: "public",
          name: "app.public",
          component: AppPublic,
          redirect: { name: "app.public.all" },
          children: [
            {
              path: "all",
              name: "app.public.all",
              component: AppPublicAll
            },
            {
              path: ":id",
              name: "app.public.book",
              component: AppPublicBook
            }
          ]
        },
        {
          path: "admin",
          name: "app.admin",
          component: AppAdmin,
          children: [
            {
              path: "books",
              name: "app.admin.books",
              component: AppAdminBooks
            },
            {
              path: "authors",
              name: "app.admin.authors",
              component: AppAdminAuthors
            }
          ]
        }
      ]
    }
  ]
});
