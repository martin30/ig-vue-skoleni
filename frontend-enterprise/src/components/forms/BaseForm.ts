import { Component, Prop, Vue, Watch, Mixins } from "vue-property-decorator";

@Component
export default class BaseForm extends Vue {
  @Prop()
  value!: any;

  form = {};

  @Watch("form", { deep: true })
  onFormChanged() {
    this.$emit("input", this.form);
  }

  @Watch("value", { deep: true })
  onValueChanged(newVal: any) {
    this.form = newVal;
  }

  created() {
    this.form = this.value;
  }
}
