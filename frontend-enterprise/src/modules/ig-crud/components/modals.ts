import { create } from "vue-modal-dialogs";
import AddNewItemModal from "./modals/AddNewItemModal.vue";
import EditItemModal from "./modals/EditItemModal.vue";
import ConfirmDeleteModal from "./modals/ConfirmDeleteModal.vue";

let addNewItemModal = create(AddNewItemModal, "formComponent");
let editItemModal = create(EditItemModal, "formComponent", "item");
let confirmDeleteModal = create(ConfirmDeleteModal);

export { addNewItemModal, editItemModal, confirmDeleteModal };
