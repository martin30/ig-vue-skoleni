import _Vue from "Vue";
import IgCrud from "./components/IgCrud.vue";

export default function(Vue: typeof _Vue) {
  Vue.component("ig-crud", IgCrud);
}
