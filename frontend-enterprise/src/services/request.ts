import axios, { AxiosRequestConfig, Method } from "axios";

export type IRequest = (
  method: Method,
  url: string,
  data?: any
) => Promise<any>;

export default (baseUrl: string) => (
  method: Method,
  url: string,
  data?: any
) => {
  let requestOptions: AxiosRequestConfig = {
    url: baseUrl + url,
    method,
    data
  };

  return axios(requestOptions).then(res => res.data);
};
