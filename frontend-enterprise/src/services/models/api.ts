import BaseRestModel from "@/services/models/BaseRestModel";
import { IRequest } from "@/services/request";

export default class {
  books: BaseRestModel;
  authors: BaseRestModel;

  constructor(private $igRequest: IRequest) {
    this.books = new BaseRestModel(this.$igRequest, "/books");
    this.authors = new BaseRestModel(this.$igRequest, "/authors");
  }
}
