import { IRequest } from "@/services/request";

export default class BaseRestModel {
  constructor(private $igRequest: IRequest, private entityName: string) {}

  create(item: any) {
    return this.$igRequest("POST", this.entityName, item);
  }

  edit(id: number, item: any) {
    return this.$igRequest("PUT", `${this.entityName}/${id}`, item);
  }

  list() {
    return this.$igRequest("GET", this.entityName);
  }

  remove(id: number) {
    return this.$igRequest("DELETE", `${this.entityName}/${id}`);
  }
}
