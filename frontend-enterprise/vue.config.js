module.exports = {
  publicPath: "/frontend-enterprise/dist/",
  lintOnSave: true,
  css: {
    sourceMap: true
  },
  productionSourceMap: false,
  chainWebpack: config => {
    config.module
      .rule("eslint")
      .use("eslint-loader")
      .options({
        fix: true
      });
  }
};
