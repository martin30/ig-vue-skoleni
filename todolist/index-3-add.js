Vue.component("AppHeader", {
  template: `
<h2>The fastest todo list header</h2>  
  `
})

Vue.component("TodoItem", {
  props: ["item"],
  template: `
<div class="border border-dark p-1 my-1">{{ item.text }}</div>  
  `
})

Vue.component("TodoList", {
  data() {
    return {
      newText: "",
      todos: [
        {
          text: "Water the plants",
          done: false
        }, {
          text: "Change the carpet",
          done: false
        }
      ]
    }
  },
  methods: {
    addClicked() {
      this.todos.push({
        text: this.newText,
        done: false
      })

      this.newText = ""
    }
  },
  template: `
<div>
<p>number of todos {{ length }}</p>
<TodoItem v-for="item in todos" :item="item" />
<div class="my-2">
  <label class="form-label">Text</label>
  <input class="form-control mb-1" v-model="newText">
  <div class="mt-2">
    <button type="button" class="btn btn-primary mt-1" @click="addClicked">Primary</button>  
  </div>
</div>
</div>  
  `
})


new Vue({
  el: "#app",
  template: `
<div class="container">
<AppHeader />
<TodoList />
</div>
  `
})
