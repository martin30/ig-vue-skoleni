Vue.component("AppHeader", {
  template: `
<h2>The fastest todo list header</h2>  
  `
})

Vue.component("TodoItem", {
  props: ["item"],
  template: `
<div :class="item.done ? 'text-success border-success' : 'border-dark'" class="border p-1 my-1" @click="$emit('done')">{{ item.text }}</div>  
  `
})

Vue.component("TodoList", {
  computed: {
    doneCount() {
      console.log("done count recount")
      return this.todos.filter(i => i.done).length
    },
  },
  data() {
    return {
      newText: "",
      todos: [
        {
          text: "Water the plants",
          done: false
        }, {
          text: "Change the carpet",
          done: false
        }
      ]
    }
  },
  methods: {
    done(item) {
      if (item.done) return;

      item.done = true;
    },
    addClicked() {
      this.todos.push({
        text: this.newText,
        done: false
      })

      this.newText = ""
    }
  },
  template: `
<div>
<p>number of todos: {{ doneCount }} / {{ todos.length }}</p>
<TodoItem v-for="item in todos" :item="item" @done="done(item)" />
<div class="my-2">
  <label class="form-label">Text</label>
  <input class="form-control mb-1" v-model="newText">
  <div class="mt-2">
    <button type="button" class="btn btn-primary mt-1" @click="addClicked">Primary</button>  
  </div>
</div>
</div>  
  `
})


new Vue({
  el: "#app",
  template: `
<div class="container">
<AppHeader />
<TodoList />
</div>
  `
})
